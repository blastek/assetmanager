import * as React from 'react';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Provider as StoreProvider } from 'react-redux';
import configureStore from './src/appRedux/store';
import App from './App';
import AppSnackbar from './src/components/AppSnackbar';
import Synchronizer from './src/components/Synchronizer';

const store = configureStore();

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
      primary: '#1D62D1',
      accent: '#AAD13B',
  }
};

export default function Main() {
    return (
      <StoreProvider store={store}>
        <PaperProvider theme={theme}>
            <App/>
            <Synchronizer/>
            <AppSnackbar/> 
        </PaperProvider>
      </StoreProvider>
    );
}
