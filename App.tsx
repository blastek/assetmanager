import 'react-native-gesture-handler';
import React, { useState, useEffect, useCallback } from "react";
import { StatusBar, Platform } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Login from './src/screens/auth/Login';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useTheme } from 'react-native-paper';
import { useAppDispatch, useAppSelector } from './src/appRedux/hooks';
import { navigationRef } from './RootNavigation';
import { AUTH_TOKEN, DB_CREATED, RELATED_ENTITIES_FETCHED } from './src/constants/LocalStorageKeys';
import BusinessAssetList from './src/screens/businessAsset/BusinessAssetList';
import BusinessAssetForm from './src/screens/businessAsset/BusinessAssetForm';
import BusinessAssetDetails from './src/screens/businessAsset/BusinessAssetDetails';
import { createDb } from './src/util/dbService';
import { setRelatedEntities } from './src/appRedux/actions';


const Stack = createNativeStackNavigator();

const App = () => {

  const { colors } = useTheme();
  const dispatch = useAppDispatch(); 
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const isUserLoggedIn = useAppSelector(state => state.auth.isUserLoggedIn);

  // Fixes small bug for android status bar.
  if (Platform.OS === 'android') {
    StatusBar.setBarStyle('light-content', true);
  }

  const checkAuth = useCallback(
    () => {
      AsyncStorage.getItem(AUTH_TOKEN).then(token => {
        if (token !== "" && token != undefined) {
          setIsAuthenticated(true);
          AsyncStorage.getItem(DB_CREATED).then((result) => {
            if (result === "1") {
              // Check if related entities (categories, statuses) are set in local db.
              AsyncStorage.getItem(RELATED_ENTITIES_FETCHED).then((result) => {
                if (result !== "1") {
                  dispatch(setRelatedEntities())
                }
              })
            }
          })      
        }
        else {
          setIsAuthenticated(false);
        }
      })

    },
    [isUserLoggedIn]
  )

  
 // Creates local SQLite database and updates local storage.
  const initDB = () => {
    AsyncStorage.getItem(DB_CREATED).then((result) => {
      if (result !== "1") {
        createDb().then(() => {
          AsyncStorage.setItem(DB_CREATED, "1")
        }).catch((error) => {
          AsyncStorage.setItem(DB_CREATED, "0")
        });
      }
    }) 
  }

  // Checks for auth token granted from Passport 
  // and sets authentication state by using local state variable.
  useEffect(() => {  
    checkAuth();
    initDB()
  }, []);

   useEffect(() => {  
    checkAuth();
   }, [checkAuth]);


  function Home() {
    return (
      <Stack.Navigator>
          <Stack.Screen name="BusinessAssetList" component={BusinessAssetList} options={{ headerShown: false }}/>
          <Stack.Screen name="BusinessAssetForm" component={BusinessAssetForm} options={{ headerShown: false }}/>
          <Stack.Screen name="BusinessAssetDetails" component={BusinessAssetDetails} options={{ headerShown: false }}/>
      </Stack.Navigator>
    );
  }

  return (
     <SafeAreaProvider>
        <NavigationContainer ref={navigationRef}>
            {Platform.OS === 'android' ? <StatusBar backgroundColor={colors.primary}/> : null}
            <Stack.Navigator>
             {!isAuthenticated ? (
               <>
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }}/>
               </>
              ) : (
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false }}/>
              )
            }  
          </Stack.Navigator> 
      </NavigationContainer>
    </SafeAreaProvider>
  );
 };

 export default App;
