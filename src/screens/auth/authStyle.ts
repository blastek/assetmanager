export default {
  container: {
    padding: 20,
    backgroundColor: 'white'
  },
  headerTxt: {
    color: 'black', 
    fontSize: 30, 
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: '15%'
  },
  form: {
    marginTop: '3%',
    padding: 30
  },
  submitBtn: {
    marginTop: '5%'
  },
  navigationBtn: {
    marginTop: '6%',
    alignSelf: 'center'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 78
  },

}