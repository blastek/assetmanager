import React, { useEffect, useState, useRef } from "react";
import { View } from 'react-native'
import { TextInput, HelperText, Button, Text, useTheme } from 'react-native-paper';
import { Formik, FormikErrors, FormikValues } from 'formik';
import { useNavigation } from '@react-navigation/native';
import { submitLogin } from '../../appRedux/auth/auth.actions';
import { useAppDispatch, useAppSelector } from "../../appRedux/hooks";
import { isValidEmail } from '../../util/utils';
import I18n from "../../i18n/i18n";
import ScreenWrapper from "../ScreenWrapper";
import authStyles from "./authStyle";
import styles from "../../style";


const Login: React.FC = () => {

  const dispatch = useAppDispatch();
  const navigation = useNavigation();
  const isUserLoggedIn = useAppSelector(state => state.auth.isUserLoggedIn);
  const isSubmitting = useAppSelector(state => state.ui.isSubmitting);
  const [isPassVisible, setPassVisible] = useState(false);
  const passwordInputRef = useRef();

  const validateForm = (values: ILogin) => {
    const errors: FormikErrors<FormikValues> = {};

    if (!values.username) {
      errors.email = I18n.t("required");
    } 
    else if (!isValidEmail(values.username)) {   
      errors.email = I18n.t("email_invalid");
    }

    if (!values.password) {
      errors.password = I18n.t("required");
    } 
    else if (values.password.length < 6 ){    
      errors.password = I18n.t("pass_length_error");
    }

    return errors;
  }

  const submitForm = (values: ILogin) => { 
    dispatch(submitLogin(values));
  }

  const onPasswordIconPress = () => {
    setPassVisible(!isPassVisible);
  }


  const initialFormValues: ILogin = { 
     username: 'testapi@tekmon.gr',
     password: 'dummyPassword88',
     client_id: 2,
     client_secret: "QFUbcpM8xZTd6wQb0XYUxR2pDg0VYI3nxlM0V1s3",
     grant_type: "password" 
    };

  return ( 
        <ScreenWrapper style={authStyles.container}>
             <Text style={authStyles.headerTxt}>
               {I18n.t("sign_in")}
             </Text>
            <Formik         
                initialValues={initialFormValues}
                validateOnBlur={false}
                validate={validateForm}
                onSubmit={submitForm}
              >
                {({
                  values,
                  errors,
                  handleChange,
                  handleBlur,
                  handleSubmit
                }) => (
              <View style={authStyles.form}>
               <View>
                   <TextInput
                      label={I18n.t("email")}
                      returnKeyType="next"
                      style={styles.textInput}
                      disabled={isSubmitting}  
                      value={values.username}
                      error={errors.username && errors.username}
                      onChangeText={handleChange('username')}
                      onBlur={handleBlur('username')}
                      onSubmitEditing={() => passwordInputRef?.current?.focus()}    
                    />
                     <HelperText
                      type="error"
                      padding="none"
                      visible={errors.username}
                    >
                      {errors.username}
                    </HelperText> 
                  </View>

                  <View>
                     <TextInput
                        label={I18n.t("password")}
                        returnKeyType="done"
                        style={styles.textInput}
                        secureTextEntry={!isPassVisible ? true : false}
                        ref={passwordInputRef}
                        disabled={isSubmitting}  
                        value={values.password}
                        error={errors.password && errors.password}
                        onChangeText={handleChange('password')}
                        onBlur={handleBlur('password')}
                        right={
                          <TextInput.Icon
                            disabled={isSubmitting}
                            forceTextInputFocus={false}
                            name={isPassVisible ? "eye-outline" : "eye-off-outline"}
                            onPress={onPasswordIconPress}     
                          />
                        }
                      />  
                      <HelperText
                        type="error"
                        padding="none"
                        visible={errors.password}
                      >
                        {errors.password}
                      </HelperText> 
                    </View>       
                   <Button 
                     mode="contained"
                     style={authStyles.submitBtn}
                     loading={isSubmitting}
                     disabled={isSubmitting}
                     onPress={handleSubmit} 
                     >
                      {I18n.t("sign_in")}
                   </Button>
                </View>     
              )}
             </Formik>

        </ScreenWrapper>
    );
}



export default Login;
