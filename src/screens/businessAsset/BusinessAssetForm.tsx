import React, { useEffect, useRef } from "react";
import { Appbar, TextInput, HelperText, Caption, FAB } from 'react-native-paper';
import { Formik, FormikErrors, FormikValues } from 'formik';
import { View, StyleSheet, Image } from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";
import { useAppDispatch, useAppSelector } from "../../appRedux/hooks";
import { useNavigation } from '@react-navigation/native';
import ScreenWrapper from "../ScreenWrapper";
import I18n from '../../i18n/i18n';
import styles from "../../style";
import TextInputAvoidingView from "./TextInputAvoidingView";
import { Picker } from "@react-native-picker/picker";
import { chooseImage } from "../../util/utils";
import { getCategories, getStatuses, storeBusinessAsset, toggleMessage } from "../../appRedux/actions";
import SpinnerOverlay from "../../components/SpinnerOverlay";


const BusinessAssetForm: React.FC = () => {

  const dispatch = useAppDispatch();
  const navigation = useNavigation();
  const formikRef = useRef();
  const categories: ICategory[] = useAppSelector(state => state.relatedEntity.categories);
  const statuses: IStatus[] = useAppSelector(state => state.relatedEntity.statuses);
  const isSubmitting: boolean = useAppSelector(state => state.ui.isSubmitting);

  const onBackPress = () => {
    navigation.goBack();
  }
  
  useEffect(() => {  
    dispatch(getCategories());
    dispatch(getStatuses());
  }, []);

  // Adds new image (base64). 
  const onChooseImagesPress = () => {
    requestAnimationFrame(() => {
      chooseImage((image: string) => {
        formikRef.current?.setFieldValue("image", image);
      })    
    });                  
  }

  const validateForm = (values: IBusinessAsset) => {
    const errors: FormikErrors<FormikValues> = {};
    if (!values.name) {
      errors.name = I18n.t("required");
    } 
    if (!values.code) {
      errors.code = I18n.t("required");
    } 
    if (!values.category) {
       dispatch(toggleMessage( {message: 'category_error',  messageType: 0} ));
    } 
    if (!values.status) {
       dispatch(toggleMessage( {message: 'status_error',  messageType: 0} ));
    } 
    if (!values.image || values.image === "") {
      dispatch(toggleMessage( {message: 'image_error',  messageType: 0} ));
    } 

    return errors;
  }

  const submitForm = (values: IBusinessAsset) => { 
     dispatch(storeBusinessAsset(values));
  }

  const initialFormValues = {
      name: "",
      code: "",
      category: 0,
      status: 0,
      description: "",
      image: ""
  }
   
  return ( 
  <View style={{ flex: 1 }}>

    <Appbar.Header>
        <Appbar.BackAction onPress={onBackPress}/>
        <Appbar.Content title={I18n.t("new_asset")}/>
        <Appbar.Action icon="check" disabled={isSubmitting} onPress={() => formikRef.current?.handleSubmit()}/>
     </Appbar.Header>

    <TextInputAvoidingView>
      <ScreenWrapper
        contentContainerStyle={{ padding: 8 }}
        keyboardShouldPersistTaps={'always'}
        removeClippedSubviews={false}
        >
         <SpinnerOverlay visible={isSubmitting}/>

        <Formik         
            innerRef={formikRef}
            initialValues={initialFormValues}
            validateOnBlur={false}
           // validateOnChange={false}
            validate={validateForm}
            onSubmit={submitForm}
          >
            {({
              values,
              errors,
              handleChange,
              handleBlur,
              setFieldValue
            }) => (
          <View style={{ flex: 1 }}>
             <View>
               <TextInput
                  label={I18n.t("name")}
                  returnKeyType="next"
                  style={styles.textInput}
                  disabled={isSubmitting}  
                  value={values.name}
                  error={errors.name && errors.name}
                  onChangeText={handleChange('name')}
                  onBlur={handleBlur('name')}
                />
                 <HelperText
                  type="error"
                  padding="none"
                  visible={errors.name}
                >
                  {errors.name}
                </HelperText>  
              </View> 

              <View>
               <TextInput
                  label={I18n.t("code")}
                  returnKeyType="done"
                  style={[styles.textInput, styles.formItem]}
                  disabled={isSubmitting}  
                  value={values.code}
                  error={errors.code && errors.code}
                  onChangeText={handleChange('code')}
                  onBlur={handleBlur('code')}
                />
                <HelperText
                  type="error"
                  padding="none"
                  visible={errors.code}
                >
                  {errors.code}
                </HelperText>
              </View> 
   
              <Caption style={styles.formItem}>{I18n.t("category")}</Caption> 
              <Picker
                    style={styles.picker}
                    itemStyle={styles.pickerItem}
                    selectedValue={values.category}
                    onValueChange={(itemValue, itemIndex) => setFieldValue("category", itemValue)}>
                    {categories.map(category => 
                        <Picker.Item key={category.id} label={category.name} value={category.id}/>
                        ) 
                    }
              </Picker> 

              <Caption style={styles.formItem}>{I18n.t("status")}</Caption> 
 
                <Picker
                  style={styles.picker}
                  itemStyle={styles.pickerItem}
                  selectedValue={values.status}
                  onValueChange={(itemValue, itemIndex) => setFieldValue("status", itemValue)}>
                  {statuses.map(status => 
                      <Picker.Item key={status.id} label={status.name_en} value={status.id}/>
                      ) 
                  }
                </Picker> 

             <View>
               <TextInput
                  label={I18n.t("description")}
                  returnKeyType="done"
                  style={[styles.textInput, styles.formItem]}
                  disabled={isSubmitting}  
                  value={values.description}
                  onChangeText={handleChange('description')}
                  onBlur={handleBlur('description')}
                />
              </View> 

            <View style={scopedStyles.row}>

    
              <Caption style={styles.formItem}>{I18n.t("image")}</Caption> 
                <FAB
                  style={scopedStyles.fab}
                  color="white"
                  icon="file-image"
                  onPress={onChooseImagesPress}
               />

            </View>

              {values.image
                 ?
                <TouchableOpacity style={styles.imageThumb} onLongPress={() => {
                    console.log("ONLONGPRESS")
                    // Remove this image.
                    setFieldValue("image", "");
                 }}>
                    <Image style={scopedStyles.img} source={ { uri: values.image } }/>
                </TouchableOpacity>
                 : null
                }
            </View>     
          )}
         </Formik>
      </ScreenWrapper>
    </TextInputAvoidingView>
  </View>
  );
}

const scopedStyles = StyleSheet.create({
  img: {
    width: 100, 
    height: 100
  },
  fab: {
    marginTop: 40
  },
  submitBtn: {
    marginTop: 35
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rowNoSpace: {
    flexDirection: 'row',
    alignItems: 'center'
  },
});

export default BusinessAssetForm;