import * as React from 'react';
import {
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { TextInput, HelperText, useTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import ScreenWrapper from '../ScreenWrapper';

 type AvoidingViewProps = {
   children: React.ReactNode;
};
  
const TextInputAvoidingView = ({ children }: AvoidingViewProps) => {
    return Platform.OS === 'ios' ? (
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior="padding"
        keyboardVerticalOffset={80}
      >
        {children}
      </KeyboardAvoidingView>
    ) : (
      <>{children}</>
    );
  };

export default TextInputAvoidingView;
  