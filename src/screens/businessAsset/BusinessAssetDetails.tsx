import React, { useEffect, useState, useCallback } from "react";
import { View, Image, StyleSheet, RefreshControl } from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";
import { Appbar, Card, Text, Caption, Badge, FAB, List } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useAppDispatch, useAppSelector } from "../../appRedux/hooks";
import { useNavigation } from '@react-navigation/native';
import ScreenWrapper from "../ScreenWrapper";
import API from '../../api/api';
import I18n from '../../i18n/i18n';
import styles from "../../style";


const BusinessAssetDetails: React.FC = ({ route }) => {

  const navigation = useNavigation();
  const [image, setImage] = useState(null);
  const { businessAsset }: { businessAsset: IBusinessAssetQueryResult} = route.params;

 // console.log( businessAsset.assetId);
   const onBackPress = () => {
    navigation.goBack();
  }


  const getImage = useCallback(
    () => {
      API.get('assets/image?id=' + businessAsset.assetId)
      .then(response => setImage(response.data))
      .catch(e => console.log("Get image error", e))
    },
    [businessAsset.assetId],
  )

  useEffect(() => {
    getImage()
  }, [getImage])

    return (

     <View style={styles.parentContainer}>

        <Appbar.Header>
          <Appbar.BackAction icon="menu" onPress={onBackPress}/>
          <Appbar.Content title={businessAsset.assetName}/>
        </Appbar.Header>

        <ScreenWrapper>
          {image != null 
           ?
             <Image style={scopedStyles.img} source={ { uri: image } }/>
           : null
          }

          <List.Item
            title={I18n.t("code")}
            description={businessAsset.assetCode}
          />
          
          <List.Item
            title={I18n.t("description")}
            description={businessAsset.assetDescription}
          />
                   
          <List.Item
            title={I18n.t("category")}
            description={businessAsset.categoryName}
          />
                             
          <List.Item
            title={I18n.t("status")}
            description={businessAsset.statusName}
          />

        </ScreenWrapper> 
     </View>

    );
}

const scopedStyles = StyleSheet.create({
  img: {
    flex: 1,
    width: '100%', 
    height: 200
  },
});

export default BusinessAssetDetails;