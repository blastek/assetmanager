import React, { useEffect, useState } from "react";
import { View, FlatList, RefreshControl } from 'react-native';
import { Card, Text, Caption, Appbar, FAB } from 'react-native-paper';
import { useAppDispatch, useAppSelector } from "../../appRedux/hooks";
import { useNavigation } from '@react-navigation/native';
import NoResults from "../../components/NoResults";
import ScreenWrapper from "../ScreenWrapper";
import I18n from '../../i18n/i18n';
import styles from "../../style";
import { listBusinessAssets, logOut, refreshBusinessAssets } from "../../appRedux/actions";


const BusinessAssetList: React.FC = () => {

   const dispatch = useAppDispatch();
   const navigation = useNavigation();
   const businessAssets = useAppSelector(state => state.businessAsset.businessAssets);
   const isLoading = useAppSelector(state => state.ui.isLoading);
   const [pageCount, setPageCount] = useState(0);

    const refresh = () => {
       dispatch(refreshBusinessAssets());
    }

   useEffect(() => {  
    dispatch(listBusinessAssets(0));
   }, []);

   const onLogOutPress = () => {
     dispatch(logOut());
   }

  const onAssetPress = (asset: IBusinessAssetQueryResult) => {
    requestAnimationFrame(() => {
      navigation.navigate('BusinessAssetDetails', { businessAsset: asset });
    });
  }

  const onCreateAssetPress = () => {
    requestAnimationFrame(() => {
      navigation.navigate('BusinessAssetForm');
    });
  }

  function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

   const keyExtractor = (item: IBusinessAssetQueryResult) => item.assetId 
            ? item.assetId + "" + item.assetName 
                 : uuid()
      
   const loadNextBatch = () => {
    const nextPage = pageCount + 1;
     dispatch(listBusinessAssets(nextPage));
     setPageCount(nextPage);
   }

   const renderItem = ({ item }: { item: IBusinessAssetQueryResult }) => { 

    return (
      <Card
          style={styles.card}
          mode='elevated' 
          elevation={5} 
          onPress={() => onAssetPress(item)}
        >
          <Card.Title title={item.assetName}/>
          <Card.Content>
              <View style={styles.row}>
                <View>
                  <Text>
                    {item.categoryName} 
                  </Text>
                  <Caption>{item.assetCode}</Caption>
                </View>
              </View>
          </Card.Content>  
      </Card>
   );
  }


    return (
      <ScreenWrapper withScrollView={false}>

        <Appbar.Header>
          <Appbar.Content title={I18n.t("assets")}/>
          <Appbar.Action icon="logout" onPress={onLogOutPress}
          />
        </Appbar.Header>

        <FlatList
          style={styles.list}
          data={businessAssets}
          refreshing={isLoading}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          initialNumToRender={20}
          maxToRenderPerBatch={30}
          onEndReached={loadNextBatch}
          ListEmptyComponent={<NoResults/>}
          refreshControl={
            <RefreshControl
              refreshing={isLoading}
              onRefresh={refresh}
            />
          }
        />
        <FAB
          style={styles.fab}
          color="white"
          icon="plus"
          onPress={onCreateAssetPress}
        />
  
      </ScreenWrapper>
    );
}


export default BusinessAssetList;