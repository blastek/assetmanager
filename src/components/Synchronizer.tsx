import React, { useEffect } from "react";
import NetInfo from "@react-native-community/netinfo";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useAppDispatch } from "../appRedux/hooks";
import { synchronizeBusinessAssets } from "../appRedux/actions";
import { AUTH_TOKEN, DB_CREATED } from "../constants/LocalStorageKeys";

const Synchronizer: React.FC = () => {

    const dispatch = useAppDispatch();

    useEffect(() => {  
       setInterval(() => {
          // Attemp sync.
          AsyncStorage.getItem(AUTH_TOKEN).then(token => {
            if (token !== "" && token != undefined) {
              NetInfo.fetch().then(state => {
                if (state.isConnected) {
                  console.log("isConnected", state.isConnected);
                  AsyncStorage.getItem(DB_CREATED).then((result) => {
                    if (result === "1") {
                      console.log("DB Ready");
                      console.log("Will SYNC")
                      dispatch(synchronizeBusinessAssets());
                    }
                  }) 
                }
              }) 
            }
          })              
        }, 120000);
  
    }, []);
      
    return null;
}

export default Synchronizer;
