import * as React from 'react';
import { ActivityIndicator, View } from 'react-native';


const Spinner = () => {
    return(
    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator
            size="large"
            style={{ marginTop: '20%' }}
        />
    </View>
   );
}

export default Spinner;
