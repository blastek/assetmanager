import React from "react";
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { useSelector } from 'react-redux';
import { Paragraph, Colors, Portal, Dialog } from 'react-native-paper';
import I18n from '../i18n/i18n';

const SpinnerOverlay = (props: any) => {

  //const message = useSelector(state => state.ui.message);

  return (
      <Portal>
        <Dialog visible={props.visible} dismissable={false}>
        <Dialog.Content>
            <View style={scopedStyles.content}>
            <ActivityIndicator
                color={Colors.indigo500}
                size='large'
                style={scopedStyles.acIndicator}
            />
            <Paragraph>{I18n.t('submitting')}</Paragraph>
            </View>
        </Dialog.Content>
        </Dialog>
    </Portal>
   );
}

const scopedStyles = StyleSheet.create({
   content: {
    flexDirection: 'row',
      alignItems: 'center'
   },
   acIndicator: {
     marginEnd: 25
   }
 });

export default SpinnerOverlay;