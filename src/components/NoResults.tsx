import React from "react";
import { StyleSheet, View, useWindowDimensions } from 'react-native';
import { Text } from 'react-native-paper'
import I18n from "../i18n/i18n";


const NoResults = () => {

    const dimensions = useWindowDimensions();
    
    const scopedStyles = StyleSheet.create({
        container: {
           flex: 1,
           justifyContent: 'center',
           alignItems: 'center',
           backgroundColor: 'white',
           height: dimensions.height - 80
        }
    });

    return (
      <View style={scopedStyles.container}>
        <Text>{I18n.t("no_results")}</Text> 
        <Text style={{ fontSize: 10 }}>{I18n.t("pull_refresh")}</Text> 
      </View>
    );
}

export default NoResults;



