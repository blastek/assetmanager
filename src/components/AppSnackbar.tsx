import React from "react";
import { StyleSheet } from 'react-native';
import { Snackbar } from 'react-native-paper'
import { toggleMessage } from "../appRedux/actions";
import { useAppDispatch, useAppSelector } from "../appRedux/hooks";
import I18n from "../i18n/i18n";


const AppSnackbar = () => {

  const dispatch = useAppDispatch();
  const isSnackbarVisible = useAppSelector(state => state.ui.isSnackbarVisible);
  const message = useAppSelector(state => state.ui.message);
  const messageType = useAppSelector(state => state.ui.messageType);

  const onSnackbarDismiss = () => {
     dispatch(toggleMessage( {message, messageType} ));
  }

  return (
    <Snackbar
       style={messageType == 0 ? scopedStyles.error : scopedStyles.success}
       visible={isSnackbarVisible}
       duration={Snackbar.DURATION_SHORT}
       onDismiss={onSnackbarDismiss}
     >
     {message !== "" ? I18n.t(message) : null}
   </Snackbar>
  );
}

const scopedStyles = StyleSheet.create({
 success: {
    backgroundColor: 'green'
 },
 error: {
    backgroundColor: 'red'
 },
});

export default AppSnackbar;