import {
    LOADER_SHOW,
    LOADER_HIDE,
    SUBMIT_INIT,
    SUBMIT_END,
    MESSAGE_TOGGLE,
  } from '../../constants/ActionTypes';
  
  const initialState = {
      isLoading: false,
      isSubmitting: false,
      isCategoryListLoading: false,
      isSnackbarVisible: false,
      messageType: 0,
      message: ""
  }
  
  export default (state = initialState, action: any) => {
    switch (action.type) {
      case SUBMIT_INIT: {
        return {
          ...state,
          isSubmitting: true
        };
      }
      case SUBMIT_END: {
        return {
          ...state,
          isSubmitting: false
        };
      }
      case LOADER_SHOW: {
        return {
          ...state,
          isLoading: true
        };
      }
      case LOADER_HIDE: {
        return {
          ...state,
          isLoading: false
        };
      }
      case MESSAGE_TOGGLE: {
        const { message, messageType } = action.payload;
        return {
          ...state,
          isSnackbarVisible: !state.isSnackbarVisible,
          message: message,
          messageType: messageType
        };
      }
      default:
        return state;
    }
  };