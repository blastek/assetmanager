import {
    LOADER_SHOW,
    LOADER_HIDE,
    SUBMIT_INIT,
    SUBMIT_END,
    MESSAGE_TOGGLE
  } from '../../constants/ActionTypes';
  
  export const showLoader = () => {
    return {
      type: LOADER_SHOW
    };
  };
  
  export const hideLoader = () => {
    return {
      type: LOADER_HIDE
    };
  };
  
  export const initSubmit = () => {
    return {
      type: SUBMIT_INIT
    };
  };
  
  export const endSubmit = () => {
    return {
      type: SUBMIT_END
    };
  };

  export const toggleMessage = (data: any) => {
    return {
      type: MESSAGE_TOGGLE,
      payload: data
    };
  };
  