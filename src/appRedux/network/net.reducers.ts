import { NETWORK_STATE_SET  } from '../../constants/ActionTypes';
  
  const initialState: NetworkState = {
    isNetworkOn: false
  }
  
  export default (state: NetworkState = initialState, action: any) => {
    switch (action.type) {
      case NETWORK_STATE_SET: {
        return {
          ...state,
          isNetworkOn: action.payload
        };
      }
      default:
        return state;
    }
  };
  