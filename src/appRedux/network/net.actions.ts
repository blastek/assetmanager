import { NETWORK_STATE_SET } from '../../constants/ActionTypes';
  
  export const setNetworkState = (networkOn: boolean | null) => {
    return {
      type: NETWORK_STATE_SET,
      networkOn
    };
  };