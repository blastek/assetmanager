import { all } from 'redux-saga/effects';
import authSagas from '../auth/auth.sagas';
import businessAssetSagas from '../businessAsset/businessAsset.sagas';
import relatedEntitySagas from '../relatedEntity/relatedEntity.sagas';


export default function* rootSaga() {
  yield all([ 
    authSagas(),
    businessAssetSagas(),
    relatedEntitySagas()
  ]);
}
