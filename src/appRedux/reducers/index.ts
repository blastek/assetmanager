import { combineReducers } from '@reduxjs/toolkit';
import UI from '../ui/ui.reducers';
import Auth from '../auth/auth.reducers';
import Network from '../network/net.reducers';
import BusinessAsset from '../businessAsset/businessAsset.reducers';
import RelatedEntity from '../relatedEntity/relatedEntity.reducers';

 const appReducer = combineReducers({
    ui: UI,
    auth: Auth,
    network: Network,
    businessAsset: BusinessAsset,
    relatedEntity: RelatedEntity
  });

const rootReducer = (state: any, action: any) => {
  if (action.type === 'LOGOUT') {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
