import {
  BUSINESS_ASSETS_CLEAR,
    BUSINESS_ASSETS_LIST,
    BUSINESS_ASSETS_LIST_SUCCESS,
    BUSINESS_ASSETS_REFRESH,
    BUSINESS_ASSETS_REFRESH_SUCCESS,
    BUSINESS_ASSETS_SYNCHRONIZE,
    BUSINESS_ASSET_SET,
    BUSINESS_ASSET_STORE,
    BUSINESS_ASSET_STORE_SUCCESS,
  } from '../../constants/ActionTypes';  
  
  export const listBusinessAssets = (page: number) => {
    return {
      type: BUSINESS_ASSETS_LIST,
      page
    };
  };
  
  export const listBusinessAssetsSuccess = (businessAssets: IBusinessAssetQueryResult[]) => {
    return {
      type: BUSINESS_ASSETS_LIST_SUCCESS,
      payload: businessAssets
    };
  };

  export const refreshBusinessAssets = () => {
    return {
      type: BUSINESS_ASSETS_REFRESH
    };
  };

  export const refreshBusinessAssetsSuccess = (businessAssets: IBusinessAssetQueryResult[]) => {
    return {
      type: BUSINESS_ASSETS_REFRESH_SUCCESS,
      payload: businessAssets
    };
  };

  export const synchronizeBusinessAssets = () => {
    return {
      type: BUSINESS_ASSETS_SYNCHRONIZE
    };
  };

  export const setBusinessAsset = (asset: IBusinessAssetQueryResult) => {
    return {
      type: BUSINESS_ASSET_SET,
      payload: asset
    };
  };

  export const storeBusinessAsset = (businessAsset: IBusinessAsset) => {
    return {
      type: BUSINESS_ASSET_STORE,
      businessAsset
    };
  };
  
  export const storeBusinessAssetSuccess =  (businessAsset: IBusinessAssetQueryResult) => {
    return {
      type: BUSINESS_ASSET_STORE_SUCCESS,
      payload: businessAsset
    };
  };

  export const clearAssets = () => {
    return {
      type: BUSINESS_ASSETS_CLEAR
    };
  };

  
  
  
  
  