import { PayloadAction } from '@reduxjs/toolkit'
import {
    BUSINESS_ASSETS_LIST_SUCCESS,
    BUSINESS_ASSETS_REFRESH_SUCCESS,
    BUSINESS_ASSET_SET,
    BUSINESS_ASSET_STORE_SUCCESS,
    BUSINESS_ASSETS_CLEAR
  } from '../../constants/ActionTypes';
  
  const initialState: BusinessAssetState = {
    businessAssets: [],
    businessAsset: {
      assetName: "",
      assetCode: "",
      assetDescription: ""
    }
  }
  
  export default (state: BusinessAssetState = initialState, action: any) => {
    switch (action.type) {
      case BUSINESS_ASSETS_LIST_SUCCESS: {
        return {
          ...state,
          businessAssets: [...state.businessAssets, ...action.payload]
        };
      }
      case BUSINESS_ASSETS_REFRESH_SUCCESS: {
        return {
          ...state,
          businessAssets: action.payload
        };
      }
      case BUSINESS_ASSET_SET: {
        return {
          ...state,
          businessAsset: action.payload
        };
      }
      case BUSINESS_ASSET_STORE_SUCCESS: {
        return {
          ...state,
          businessAssets: [...state.businessAssets, action.payload]
        };
      }
      case BUSINESS_ASSETS_CLEAR: {
        return {
          ...state,
          businessAssets: []
        };
      }
      default:
        return state;
    }
  };