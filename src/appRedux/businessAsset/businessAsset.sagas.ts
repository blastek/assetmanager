import {
    all,
    call,
    select,
    fork,
    put,
    takeEvery
} from 'redux-saga/effects';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    BUSINESS_ASSETS_LIST,
    BUSINESS_ASSETS_REFRESH,
    BUSINESS_ASSETS_SYNCHRONIZE, 
    BUSINESS_ASSET_STORE
} from "../../constants/ActionTypes";
import { listBusinessAssetsSuccess, refreshBusinessAssets, refreshBusinessAssetsSuccess, setBusinessAsset, storeBusinessAssetSuccess, synchronizeBusinessAssets } from './businessAsset.actions';
import { endSubmit, hideLoader, initSubmit, showLoader, toggleMessage } from '../actions';
import { LAST_SYNCHRONIZED_AT } from '../../constants/LocalStorageKeys';
import { AxiosResponse } from 'axios';
import { getAssetResults, getTempAssetResults, getTempAssetsForSync, insertBusinessAsset, insertTempBusinessAsset, populateAssetsTable, removeAllRows } from '../../util/dbService';
import { isConnectedToNetwork } from '../../util/utils';
import API from '../../api/api';


const getLastSynchronization = async () => await AsyncStorage.getItem(LAST_SYNCHRONIZED_AT).then(result => result);
const getNowStr = () => new Date().toISOString().slice(0, 19).replace('T', ' ');
const updateLastSynchronization = async () =>await  AsyncStorage.setItem(LAST_SYNCHRONIZED_AT, getNowStr());

const submitTempAssets = (businessAssets: IBusinessAsset[]): Promise<void> => {
    return new Promise((resolve, reject) => {
      let promises: Promise<any>[] = [];
      businessAssets.forEach(businessAsset =>
        promises.push(
          new Promise((resolve, reject) => API.post('assets/create', businessAsset)
            .then(res => resolve(true)))
            .catch(e => console.log("Failed asset name" + businessAsset.name))
          )
      );
 
      Promise.all(promises).then(result => resolve()).catch(e => reject());
    });
  }

function* synchronizeBusinessAssetsRequest() {
    console.log("synchronizeBusinessAssetsRequest")
    try {
        // Send data from temp table.
        const tempAssets: any[] = yield call(getTempAssetsForSync);
        if (tempAssets.length > 0) {
         // console.log("tempAssets length", tempAssets.length)
          yield call(submitTempAssets, tempAssets)
          // Remove temp assets from local database.
          yield call(removeAllRows, 'Assets_Temp');
        }

      // Synchronize all assets.
      let ts = "1970-01-01 23:59:59";
      const lastSunchronizedAt: string = yield call(getLastSynchronization);
      if (lastSunchronizedAt !== "" && lastSunchronizedAt != undefined) {
        ts = lastSunchronizedAt;
      }
      console.log("ts", ts);
      const response: AxiosResponse = yield API.get('assets/all', { params: { 'ts': ts } });
      const businessAssets = response.data;
      console.log("--- Synchronized assets length: ", businessAssets.length);
      if (businessAssets.length > 0) {
        yield call (populateAssetsTable, businessAssets);
      }
      yield call(updateLastSynchronization);
      yield put(refreshBusinessAssets());
    } catch (error) {
        console.log("synchronizeBusinessAssetsRequest", error)
    }
}


function* listBusinessAssetsRequest({ page }: { page: number}) {
  console.log("listBusinessAssetsRequest", page)
    try {
        yield put(showLoader());
        const assetResults: IBusinessAssetQueryResult[] = yield call(getAssetResults, page);
        const tempAssetResults: IBusinessAssetQueryResult[] = yield call(getTempAssetResults);
        yield put(hideLoader());
        if (assetResults.length > 0 || tempAssetResults.length > 0) {
          yield put(listBusinessAssetsSuccess([...tempAssetResults, ...assetResults]));
        }
      } catch (error) {
         yield put(hideLoader());
         yield put(toggleMessage( {message: 'error',  messageType: 0} ));
         console.log(error)
    }
}

function* refreshBusinessAssetsRequest() {
    try {
        yield put(showLoader());
        const assetResults: IBusinessAssetQueryResult[] = yield call(getAssetResults, 0);
        const tempAssetResults: IBusinessAssetQueryResult[] = yield call(getTempAssetResults);
        yield put(hideLoader());
        yield put(refreshBusinessAssetsSuccess([...tempAssetResults, ...assetResults]));
    } catch (error) {
         yield put(hideLoader());
         yield put(toggleMessage( {message: 'error',  messageType: 0} ));
         console.log(error)
    }
}

function* storeBusinessAssetRequest({ businessAsset }: { businessAsset: IBusinessAsset }) {
  console.log("storeBusinessAssetRequest", businessAsset)
  try {
      yield put(initSubmit());
       const isConnected: boolean = yield call(isConnectedToNetwork);
      if (isConnected) { // Is connected so submit data to backend.
          const response: AxiosResponse = yield API.post('assets/create', businessAsset);
          yield put(endSubmit());
          yield call(insertBusinessAsset, { ...businessAsset, id: response.data.asset_id }); 
          yield put(refreshBusinessAssets()); 
        }
      else { // No connection, store in SQLite.
         businessAsset.created_at = yield call(getNowStr);
         insertTempBusinessAsset(businessAsset);
         yield put(refreshBusinessAssets()); 
      }
      yield put(toggleMessage( {message: 'asset_stored',  messageType: 1} ));
      yield put(endSubmit());
  } catch (error) {
      console.log(error)
      yield put(endSubmit());
      yield put(toggleMessage( {message: 'error',  messageType: 0} ));
  }
}


export function* businessAssetsSynchronizationWatcher() {
    yield takeEvery(BUSINESS_ASSETS_SYNCHRONIZE, synchronizeBusinessAssetsRequest);
}

export function* businessAssetsListWatcher() {
    yield takeEvery(BUSINESS_ASSETS_LIST, listBusinessAssetsRequest);
}

export function* businessAssetsRefreshWatcher() {
  yield takeEvery(BUSINESS_ASSETS_REFRESH, refreshBusinessAssetsRequest);
}

export function* businessAssetStoreWatcher() {
  yield takeEvery(BUSINESS_ASSET_STORE, storeBusinessAssetRequest);
}

export default function* rootSaga() {
    yield all([
      fork(businessAssetsSynchronizationWatcher),
      fork(businessAssetsListWatcher),
      fork(businessAssetsRefreshWatcher),
      fork(businessAssetStoreWatcher),
    ]);
}
