import {
    all,
    call,
    fork,
    put,
    takeEvery
} from 'redux-saga/effects';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    CATEGORIES_GET,
    RELATED_ENTITIES_SET,
    STATUSES_GET
} from "../../constants/ActionTypes";
import API from '../../api/api';
import { getCategoriesSuccess, getStatusesSuccess } from './relatedEntity.actions';
import { getLocalCategories, getLocalStatuses, populateCategoriesTable, populateStatusesTable } from '../../util/dbService';
import { AxiosResponse } from 'axios';
import { RELATED_ENTITIES_FETCHED } from '../../constants/LocalStorageKeys';
import { isConnectedToNetwork } from '../../util/utils';

const updateLocalStorage = async () => await AsyncStorage.setItem(RELATED_ENTITIES_FETCHED, "1")

function* getCategoriesRequest() {
    try {
        const isConnected: boolean = yield call(isConnectedToNetwork);
        let data: ICategory[] = [];
        if (isConnected) {
          const response: AxiosResponse = yield API.get('assets/categories/all'); 
          data = response.data;
        }
        else {
          data = yield call(getLocalCategories);
        }
       yield put(getCategoriesSuccess(data))
    } catch (error) {
        console.log(error)
    }
}

function* getStatusesRequest() {
    try {
        const isConnected: boolean = yield call(isConnectedToNetwork);
        let data: IStatus[] = [];
        if (isConnected) {
          const response: AxiosResponse = yield API.get('assets/statuses/all'); 
          data = response.data;
        }
        else {
         data = yield call(getLocalStatuses);
        }
        yield put(getStatusesSuccess(data));
    } catch (error) {
        console.log(error)
    }
}


function* setRelatedEntitiesRequest() {
    try {
        const statusesResponse: AxiosResponse = yield API.get('assets/statuses/all'); 
        populateStatusesTable(statusesResponse.data)
        const categoriesResonse: AxiosResponse = yield API.get('assets/categories/all'); 
        populateCategoriesTable(categoriesResonse.data);
        yield call(updateLocalStorage);
      //  yield put(synchronizeBusinessAssets());
    } catch (error) {
        console.log(error)
    }
}


export function* relatedEntitiesSetWatcher() {
    yield takeEvery(RELATED_ENTITIES_SET, setRelatedEntitiesRequest);
}

export function* categoriesGetWatcher() {
    yield takeEvery(CATEGORIES_GET, getCategoriesRequest);
}

export function* statusesGetWatcher() {
    yield takeEvery(STATUSES_GET, getStatusesRequest);
}

export default function* rootSaga() {
    yield all([
      fork(relatedEntitiesSetWatcher),
      fork(categoriesGetWatcher),
      fork(statusesGetWatcher)
    ]);
}
