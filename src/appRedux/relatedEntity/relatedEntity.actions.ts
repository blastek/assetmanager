import {
    CATEGORIES_GET,
    CATEGORIES_GET_SUCCESS,
    STATUSES_GET,
    STATUSES_GET_SUCCESS,
    RELATED_ENTITIES_SET
  } from '../../constants/ActionTypes';


  export const setRelatedEntities = () => {
    return {
      type: RELATED_ENTITIES_SET
    };
  };

  export const getCategories = () => {
    return {
      type: CATEGORIES_GET
    };
  };

  export const getCategoriesSuccess = (categories: ICategory[]) => {
    return {
      type: CATEGORIES_GET_SUCCESS,
      payload: categories
    };
  };

  export const getStatuses = () => {
    return {
      type: STATUSES_GET
    };
  };

  export const getStatusesSuccess = (statuses: IStatus[]) => {
    return {
      type: STATUSES_GET_SUCCESS,
      payload: statuses
    };
  };
  