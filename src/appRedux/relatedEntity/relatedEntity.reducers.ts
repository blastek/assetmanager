import {
  CATEGORIES_GET_SUCCESS,
    STATUSES_GET_SUCCESS
  } from '../../constants/ActionTypes';
  
  const initialState: RelatedEntitiesState = {
    categories: [],
    statuses: []
  }
  
  export default (state: RelatedEntitiesState = initialState, action: any) => {
    switch (action.type) {
      case CATEGORIES_GET_SUCCESS: {
        return {
          ...state,
          categories: action.payload
        };
      }
      case STATUSES_GET_SUCCESS: {
        return {
          ...state,
          statuses: action.payload
        };
      }
      default:
        return state;
    }
  };