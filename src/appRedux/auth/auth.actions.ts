import {
  LOGIN_SUBMIT,
  LOGIN_SUCCESS,
  LOGOUT,
  LOGOUT_SUCCESS
} from '../../constants/ActionTypes';

export const submitLogin = (data: ILogin) => {
  return {
    type: LOGIN_SUBMIT,
    data
  };
};

export const loginSuccess = () => {
  return {
    type: LOGIN_SUCCESS
  };
};

export const logOut = () => {
  return {
    type: LOGOUT
  };
};

export const logOutSuccess = () => {
  return {
    type: LOGOUT_SUCCESS
  };
};






