import {
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS
} from '../../constants/ActionTypes';

const initialState: AuthState = {
  isUserLoggedIn: false
}

export default (state: AuthState = initialState, action: any) => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      return {
        ...state,
        isUserLoggedIn: true
      };
    }
    case LOGOUT_SUCCESS: {
      return {
        ...state,
        isUserLoggedIn: false
      };
    }
    default:
      return state;
  }
};
