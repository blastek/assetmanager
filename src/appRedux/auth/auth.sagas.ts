import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import {
   LOGIN_SUBMIT ,
   LOGOUT
} from '../../constants/ActionTypes';
import {
   loginSuccess,
   logOutSuccess
  } from './auth.actions';
import { AUTH_TOKEN } from '../../constants/LocalStorageKeys';
import axios, { AxiosResponse } from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { baseUrl } from '../../api/baseUrl';
import { clearAssets, endSubmit, initSubmit, toggleMessage } from '../actions';
import { isConnectedToNetwork } from '../../util/utils';

const removeToken = async () => await AsyncStorage.removeItem(AUTH_TOKEN);

const updateLocalStorage = async (data: TokenData) => {
  await AsyncStorage.setItem(AUTH_TOKEN, data.access_token);
}

function* loginRequest({ data }: any) {
    try {
      const isConnected: boolean = yield call(isConnectedToNetwork);
      if (isConnected) {
        yield put(initSubmit());
        const response: AxiosResponse = yield axios.post(baseUrl + '/oauth/token', data);
        yield call(updateLocalStorage, response.data);
        yield put(loginSuccess());
        yield put(endSubmit());
      }
      else {
        // Show message
        yield put(toggleMessage( {message: 'no_internet',  messageType: 0} ));
      } 
    } catch (error: any) {
      yield put(endSubmit());
      if (error.response.status === 400) {
        yield put(toggleMessage( {message: 'wrong_creds',  messageType: 0} ));
      }
      else {
        yield put(toggleMessage( {message: 'error',  messageType: 0} ));
      }
    }
}

function* logOut() {
  yield call(removeToken);
  yield put(clearAssets());
  yield put(logOutSuccess());
}


export function* loginSubmitWatcher() {
  yield takeEvery(LOGIN_SUBMIT, loginRequest);
}

export function* logOutWatcher() {
  yield takeEvery(LOGOUT, logOut);
}


export default function* rootSaga() {
  yield all([
    fork(loginSubmitWatcher),
    fork(logOutWatcher)
  ]);
}