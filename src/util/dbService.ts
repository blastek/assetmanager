import SQLite, { SQLiteDatabase, Transaction } from 'react-native-sqlite-storage';

SQLite.DEBUG(true);
SQLite.enablePromise(true);
const dbName = "bAassetManager.db";
const version = "1.0";
const alias = "App DB";
let db: SQLiteDatabase;
const limit: number = 20;

 export const createDb = (): Promise<void> => {
    return new Promise((resolve, reject) => {
      SQLite.openDatabase(dbName, version, alias)
           .then((DB) => {
            DB.executeSql('SELECT 1 FROM Version LIMIT 1').then(() =>{
              resolve();
            }).catch((error: any) => {
              DB.transaction(createTables).then(() => {
                resolve();
              }).catch((error) => {
                reject();
              });
            });
        }).catch((error) => {
          reject();
        });
    });
  } 

  const createTables = (tx: Transaction) => {

    tx.executeSql('CREATE TABLE IF NOT EXISTS Version( '
    + 'version_id INTEGER PRIMARY KEY NOT NULL); ').catch((error: any) => {
        Promise.reject(error);
    });

    tx.executeSql('CREATE TABLE IF NOT EXISTS Statuses( '
        + 'id INTEGER PRIMARY KEY NOT NULL, '
        + 'name_en VARCHAR(150), '
        + 'name_gr VARCHAR(150), '
        + 'deleted INTEGER, '
        + 'created_at VARCHAR(40), '
        + 'updated_at VARCHAR(40) ); ').catch((error: any) => {
            Promise.reject(error);
     });

     tx.executeSql('CREATE TABLE IF NOT EXISTS Categories( '
        + 'id INTEGER PRIMARY KEY NOT NULL, '
        + 'name VARCHAR(150), '
        + 'deleted INTEGER, '
        + 'created_at VARCHAR(40), '
        + 'updated_at VARCHAR(40) ); ').catch((error: any) => {
          Promise.reject(error);
    });

      tx.executeSql('CREATE TABLE IF NOT EXISTS Assets( '
        + 'id INTEGER PRIMARY KEY NOT NULL, '
        + 'name VARCHAR(150), '
        + 'code VARCHAR(150), '
        + 'description VARCHAR(240), '
        + 'status INTEGER, '
        + 'category INTEGER, '
        + 'deleted INTEGER, '
        + 'image TEXT, '
        + 'created_at VARCHAR(40), '
        + 'updated_at VARCHAR(40), '
        + 'FOREIGN KEY ( status ) REFERENCES Statuses ( id ) '
        + 'FOREIGN KEY ( category ) REFERENCES Categories ( id ) ); ').catch((error: any) => {
          Promise.reject(error);
     });

     // Used by assets form for offline mode.
     tx.executeSql('CREATE TABLE IF NOT EXISTS Assets_Temp( '
     + 'name VARCHAR(150), '
     + 'code VARCHAR(150), '
     + 'description VARCHAR(240), '
     + 'status INTEGER, '
     + 'category INTEGER, '
     + 'image TEXT, '
     + 'created_at VARCHAR(40) ); ').catch((error: any) => {
       Promise.reject(error);
  });
  }

  const setDB = (): Promise<void> => {
    return new Promise((resolve, reject) => {
      if (!db) {
        SQLite.openDatabase(dbName).then((DB) => {
           db = DB;
          resolve();
         }).catch((error) => {
           console.log("---- setDB Error", error)
           reject();
         });
       }
       else {
          resolve();
      }
    });
  }

  export const populateStatusesTable = (statuses: IStatus[]) => {
      console.log("Populate statuses")
      setDB().then(() => {
         db.transaction((tx: Transaction) => {
            statuses.map((status: IStatus) => {
              tx.executeSql('INSERT INTO Statuses (id, name_en, name_gr, created_at, updated_at) VALUES(?,?,?,?,?)',
              [status.id, status.name_en, status.name_gr, status.created_at, status.updated_at]);
            })
          })
       })
  }

 export const populateCategoriesTable = (categories: ICategory[]) => {
    console.log("Populate categories table")
    setDB().then(() => {
        db.transaction((tx: Transaction) => {
           categories.map((category: ICategory) => {
             tx.executeSql('INSERT INTO Categories (id, name, created_at, updated_at) VALUES (?,?,?,?)',
             [category.id, category.name, category.created_at, category.updated_at]);
           })
        })
    }).catch((error) => {
        console.log(error);
    });
}

export const populateAssetsTable = (assets: IBusinessAsset[]) => {
  console.log("Populate assets table")
    setDB().then(() => {
        db.transaction((tx: Transaction) => {
          assets.map((asset: IBusinessAsset) => {
            tx.executeSql('INSERT INTO Assets (id, name, code, description, deleted, category, status, created_at, updated_at)'
            + ' VALUES (?,?,?,?,?,?,?,?,?)',
            [asset.id, asset.name, asset.code, asset.description, asset.deleted,
              asset.category, asset.status, asset.created_at, asset.updated_at])
              .then(res => console.log("INSERTION RESULT", res))
              .catch(error => console.log("INSERTION ERROR", error))
              ;

          })
        })
    }).catch((error) => {
        console.log("populateAssetsTable ERROR", error)
    });
}

export const insertBusinessAsset = (asset: IBusinessAsset): Promise<void> => {
   return new Promise((resolve, reject) => {
      setDB().then(() => {
        db.transaction((tx: Transaction) => {
            tx.executeSql('INSERT INTO Assets (id, name, code, description, deleted, category, status, created_at, updated_at)'
            + ' VALUES (?,?,?,?,?,?,?,?,?)',
            [asset.id, asset.name, asset.code, asset.description, asset.deleted,
              asset.category, asset.status, asset.created_at, asset.updated_at]);
            resolve();
        })
      }).catch((error) => {
        //  console.log(error);
          reject();
      });
   });
}


export const insertTempBusinessAsset = (asset: ITempBusinessAsset): Promise<void> => {
  return new Promise((resolve, reject) => {
    setDB().then(() => {
      db.transaction((tx: Transaction) => {
          tx.executeSql('INSERT INTO Assets_Temp (name, code, description, category, status, image)'
          + ' VALUES (?,?,?,?,?,?)',
          [asset.name, asset.code, asset.description, asset.category, asset.status, asset.image]);
          resolve();
      })
    }).catch((error) => {
        reject();
     //   console.log(error);
    });
  });
}

export const getAssetResults = (page: number) => {
  return new Promise((resolve, reject) => {
    setDB().then(() => {
        db.transaction((tx: Transaction) => {
            tx.executeSql('SELECT Assets.id as assetId, Assets.name as assetName, Assets.code as assetCode, Assets.description as assetDescription'
            + ', Categories.name as categoryName '
            + ', Statuses.name_en as statusName from Assets '
            + 'JOIN Categories ON Assets.category=Categories.id '
            + 'JOIN Statuses ON Assets.status=Statuses.id '
            + 'ORDER BY Assets.id DESC'
            + ' LIMIT ' + limit + ' offset ' + (page * limit))
            .then(([tx, results]) => {
                var len = results.rows.length;
                let tableResults = [];
                for (let i = 0; i < len; i++) {
                 tableResults.push(results.rows.item(i));
                }
                resolve(tableResults);
              }).catch(e => reject());
            }).catch(e => reject());
          }).catch((error) => {
             reject();
        });
    });
  }

  export const getTempAssetResults = () => {
    return new Promise((resolve, reject) => {
      setDB().then(() => {
           db.transaction((tx: Transaction) => {
              tx.executeSql('SELECT Assets_Temp.name as assetName, Assets_Temp.code as assetCode, Assets_Temp.description as assetDescription'
              + ', Categories.name as categoryName '
              + ', Statuses.name_en as statusName from Assets_Temp '
              + 'JOIN Categories ON Assets_Temp.category=Categories.id '
              + 'JOIN Statuses ON Assets_Temp.status=Statuses.id '
              + 'ORDER BY Assets_Temp.created_at DESC '
              + 'LIMIT ' + limit)
              .then(([tx, results]) => {
                  var len = results.rows.length;
                  let tableResults = [];
                  for (let i = 0; i < len; i++) {
                   tableResults.push(results.rows.item(i));
                  }
                  resolve(tableResults);
              }).catch(e => reject());
            }).catch(e => reject());
          }).catch((error) => {
          reject();
        });
      });
   }

  export const getTempAssetsForSync = () => {
    return new Promise((resolve, reject) => {
      setDB().then(() => {
            db.transaction((tx: Transaction) => {
              tx.executeSql('SELECT Assets_Temp.name, Assets_Temp.code, Assets_Temp.description'
              + ', Assets_Temp.category, Assets_Temp.status, Assets_Temp.image FROM Assets_Temp').then(([tx, results]) => {
                  var length = results.rows.length;
                  let tableResults = [];
                  for (let i = 0; i < length; i++) {
                   tableResults.push(results.rows.item(i));
                  }
                  resolve(tableResults);
              });
            }).catch(e => reject());
          }).catch((error) => {
          reject();
        });
      });
   }

  export const removeAllRows = (tableName: string): Promise<void> => {
    return new Promise((resolve, reject) => {
      setDB().then(() => {
          db.transaction((tx: Transaction) => {
              tx.executeSql('DELETE FROM ' + tableName).then(([tx, results]) => {
                  resolve();
              });
            })
        }).catch((error) => {
          reject();
        });
      });
   }

  export const getLocalCategories = () => {
    return new Promise((resolve, reject) => {
      setDB().then(() => {
          db.transaction((tx: Transaction) => {
              tx.executeSql('SELECT * from Categories').then(([tx, results]) => {
                  var len = results.rows.length;
                  let tableResults = [];
                  for (let i = 0; i < len; i++) {
                   tableResults.push(results.rows.item(i));
                  }
                  resolve(tableResults);
              });
            })
        }).catch((error) => {
          console.log(error);
        });
      });
    }

  export const getLocalStatuses = () => {
    return new Promise((resolve, reject) => {
      setDB().then(() => {
          db.transaction((tx: Transaction) => {
              tx.executeSql('SELECT * from Statuses').then(([tx, results]) => {
                  var len = results.rows.length;
                  let tableResults = [];
                  for (let i = 0; i < len; i++) {
                    tableResults.push(results.rows.item(i));
                  }
                  resolve(tableResults);
              });
            })
        }).catch((error) => {
          console.log(error);
        });
      });
    }


export const dropTables = () => {
  setDB().then(() => {
      db.transaction((tx: Transaction) => {
          logMessage("Executing DROP stmts");
          tx.executeSql('DROP TABLE IF EXISTS Version;');
          tx.executeSql('DROP TABLE IF EXISTS Statuses;');
          tx.executeSql('DROP TABLE IF EXISTS Categories;');
          tx.executeSql('DROP TABLE IF EXISTS Assets;');
          tx.executeSql('DROP TABLE IF EXISTS Assets_Temp;');
      });
    }).catch((error) => {
      console.log(error);
    });
}


  // const closeDatabase = () => {
  //   if (db) {
  //     console.log("Closing database ...");
  //     logMessage("Closing DB");
  //     db.close().then((status: any) => {
  //       logMessage("Database CLOSED");
  //     }).catch((error: any) => {
  //       Promise.reject(error);
  //     });
  //   } else {
  //     logMessage("Database was not OPENED")
  //   }
  // };

  const logMessage = (message: string) => {
      console.log(message);
  }

  
