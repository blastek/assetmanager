import NetInfo from "@react-native-community/netinfo";
import { launchImageLibrary } from 'react-native-image-picker';

export const isValidEmail = (email: string) => {
  return /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email);
}

export const isNumeric = (value: string) => {
  return /^[0-9\b]+$/.test(value);
}

export const isConnectedToNetwork = () => NetInfo.fetch().then(state => state.isConnected);

export const chooseImage = (callback: any) => {
    launchImageLibrary({mediaType: 'photo', includeExtra: true, includeBase64: true}, (response) => {
      if (response.didCancel) {
        console.log("--- ImagePicker", 'User cancelled image picker');
      }
      else if (response.error) {
        console.log("--- ImagePicker", 'ImagePicker Error: ', response.error);
     } 
     else if (response.customButton) {
       console.log("--- ImagePicker", 'User tapped custom button: ', response.customButton);
     }
     else {
     // console.log(response.assets[0].base64);
       callback("data:image/png;base64," + response.assets[0].base64);
      }
    });
  }