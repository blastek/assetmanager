interface ILogin {
    client_id: number;
    client_secret: string;
    grant_type: string;
    username: string;
    password: string;
}

type AuthState = {
    isUserLoggedIn: boolean;
}

interface TokenData {
    access_token: string;
    token_type: string;
}