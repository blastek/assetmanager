interface IBusinessAsset {
    id: number;
    name: string;
    code: string;
    description?: string;
    deleted?: number;
    image?: string;
    category?: number;
    status?: number;
    created_at?: string;
    updated_at?: string;
}

interface ITempBusinessAsset {
    name?: string;
    code?: string;
    description?: string;
    image?: string;
    category?: number;
    status?: number;
}

interface IBusinessAssetQueryResult {
    assetId?: number;
    assetName: string
    assetCode: string
    assetDescription?: string;
    statusName?: string;
    categoryName?: string;
}

interface IStatus {
    id: number;
    name_en: string;
    name_gr: string;
    created_at: string;
    updated_at: string;
}

interface ICategory {
    id: number;
    name: string;
    deleted: boolean;
    created_at: string;
    updated_at: string;
}

type BusinessAssetState = {
    businessAssets: IBusinessAsset[];
    businessAsset: IBusinessAssetQueryResult;
}

type RelatedEntitiesState = {
    categories: ICategory[];
    statuses: IStatus[];
}



