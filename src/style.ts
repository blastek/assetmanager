import { StyleSheet, Platform } from 'react-native';


const styles = StyleSheet.create({
   parentContainer: {
     flex: 1,
     backgroundColor: 'white'
   },
   content: {
     flex: 1,
     paddingHorizontal: 12,
     paddingVertical: 12,
     backgroundColor: 'white'
   },
   list: {
    padding: 2
   },
    textInput: {
      backgroundColor: 'transparent',
      paddingHorizontal: 0
    },
    row: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: 10
    },
    card: {
      margin: 8,
      borderRadius: 15,
    },
    formItem: {
      marginTop: 23
    },
    sectionHeader: {
      marginStart: 8,
      marginTop: 12
    },
    map: {
      width: '100%',
      height: 150,
      marginTop: 20
    },
    picker: {
      height: Platform.OS === 'ios' ? 110 : null,
    },
    pickerItem: {
      height: Platform.OS === 'ios' ? 110 : null,
    },
    halfRow: {
      flex: 1,
      flexDirection: 'column',
      minWidth: '50%',
      paddingHorizontal: 8,
      paddingVertical: 5
    },
    separator: {
      marginTop: 12
    },
    linedUp: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16
    },
    fab: {
      position: 'absolute',
      margin: 20,
      right: 0,
      bottom: 0
    },
    inlineFormSeparator: {
      width: '2%'
    },
    fullRow: {
      flex: 1,
      flexWrap: 'wrap',
      flexDirection: 'row'
    },
    fullRowChild: {
      flexBasis: '49%'
    },
    imageThumb: {
      marginHorizontal: 5
    },
    centerdText: {
      justifyContent: "center", 
      alignItems: "center"
    },
    detailsItem: {
      marginTop: 15
    },
    customError: {
      fontSize: 12,
      color: "#B00020"
     },
     cardBadge: {
      position: 'absolute', 
      top: 15,
      end: 18
    },
    userIcon: {
      color: '#c4c4c4',
      fontSize: 100,
      alignSelf: 'center'
    },
    successIcon: {
      color: '#00b832',
      fontSize: 130,
      marginTop: '8%'
    },
    successHeaderTxt: {
      textAlign: 'center',
      fontSize: 22,
      marginTop: 10
    }
});

export default styles;
