import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AUTH_TOKEN } from '../constants/LocalStorageKeys';
import { baseUrl } from './baseUrl';

const axiosInstance = axios.create({
  baseURL: baseUrl + '/api'
});

axiosInstance.interceptors.request.use(
  async config => {
    config.headers['Content-Type'] = 'application/json';
    config.headers['Authorization'] = 'Bearer ' + await AsyncStorage.getItem(AUTH_TOKEN);
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  response => {
    // Successful response.
    // console.log('response data', response.data);
    // console.log('response status', response.status);

    return response;
  },
  error => {
    // Response error.
    // console.log(error);
    console.log(error.response.data.errors);
  //  console.log("status", error.response.status);
    return Promise.reject(error);
  }
);

export default axiosInstance;
